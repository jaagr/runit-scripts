#!/bin/sh
# Quagga service-multiplexing script - don't run directly
set -e
service=${PWD##*/}

test "$service" = zebra || sv start zebra
install -m 0750 -o quagga -g quagga -d /run/quagga

exec /usr/sbin/"$service" -f /etc/quagga/"$service".conf
