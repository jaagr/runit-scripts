#!/bin/sh
# Basic log/run script, for things that need one. Logs to /var/log/runit/$servicedirname/.
main_service=${PWD%/*}
logdir=/var/log/runit/${main_service##*/}
install -d $logdir
exec svlogd -ttt $logdir
